<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Route::get('/', function () {
//     return view('about');
// });
// Dengan Parameter
// Route::get('/', function () {
//     return view('about', ['name' => 'Ichal'] );
// });


// Best Practice route
Route::view('/about', 'about');

// Dengan parameter
// Route::view('/about', 'about' , ['name' => 'Ichal'] );


Route::prefix('prefixroute')->group(function ()
{
    Route::get('users/{id}', function ($id) {
        return 'userid'.$id;
    });
    Route::view('/aboutme', 'about.aboutus', ['name' => 'ichal']);
});